# SME-Sem-B-Vortrag

Meine Präsentation für das SME Seminar "Künstliche Intelligenz: Gestern, heute, morgen".

## Building

Um die PDF zu bauen, muss eine `LaTeX` Umgebung installiert sein, anschließend im Wurzelverzeichnis 
`latexmk` eingeben. Die gebaute PDF findest du dann im `build` Ordner.